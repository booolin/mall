package com.linbo.mall.product.dao;

import com.linbo.mall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 22:08:25
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
