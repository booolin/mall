package com.linbo.mall.coupon.dao;

import com.linbo.mall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:08:03
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
