package com.linbo.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.coupon.entity.SeckillSkuNoticeEntity;

import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:08:03
 */
public interface SeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

