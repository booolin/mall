package com.linbo.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.coupon.entity.CouponSpuRelationEntity;

import java.util.Map;

/**
 * 优惠券与产品关联
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:08:03
 */
public interface CouponSpuRelationService extends IService<CouponSpuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

