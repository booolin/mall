package com.linbo.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:08:03
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

