package com.linbo.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.member.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:14:51
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

