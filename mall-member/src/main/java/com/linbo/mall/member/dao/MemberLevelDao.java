package com.linbo.mall.member.dao;

import com.linbo.mall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:14:51
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
