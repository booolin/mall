package com.linbo.mall.order.dao;

import com.linbo.mall.order.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:20:14
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
