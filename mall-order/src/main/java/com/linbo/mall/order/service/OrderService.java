package com.linbo.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:20:14
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

