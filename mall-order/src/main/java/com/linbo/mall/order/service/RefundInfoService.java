package com.linbo.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.order.entity.RefundInfoEntity;

import java.util.Map;

/**
 * 退款信息
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:20:14
 */
public interface RefundInfoService extends IService<RefundInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

