package com.linbo.mall.order.dao;

import com.linbo.mall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:20:14
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
