package com.linbo.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.order.entity.MqMessageEntity;

import java.util.Map;

/**
 * 
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:20:14
 */
public interface MqMessageService extends IService<MqMessageEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

