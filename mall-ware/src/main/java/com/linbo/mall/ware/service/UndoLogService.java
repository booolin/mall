package com.linbo.mall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.ware.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:23:47
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

