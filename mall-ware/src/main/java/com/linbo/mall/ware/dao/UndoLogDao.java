package com.linbo.mall.ware.dao;

import com.linbo.mall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:23:47
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
