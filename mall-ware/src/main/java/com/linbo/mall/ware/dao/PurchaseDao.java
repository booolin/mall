package com.linbo.mall.ware.dao;

import com.linbo.mall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:23:47
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
