package com.linbo.mall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linbo.common.utils.PageUtils;
import com.linbo.mall.ware.entity.WareSkuEntity;

import java.util.Map;

/**
 * 商品库存
 *
 * @author linbo
 * @email imlinbo@live.com
 * @date 2021-05-11 23:23:47
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

